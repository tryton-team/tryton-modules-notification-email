Metadata-Version: 2.1
Name: trytond-notification-email
Version: 7.0.0
Summary: Tryton module for sending email notifications
Home-page: http://www.tryton.org/
Download-URL: http://downloads.tryton.org/7.0/
Author: Tryton
Author-email: foundation@tryton.org
License: GPL-3
Project-URL: Bug Tracker, https://bugs.tryton.org/
Project-URL: Documentation, https://docs.tryton.org/
Project-URL: Forum, https://www.tryton.org/forum
Project-URL: Source Code, https://code.tryton.org/tryton
Keywords: tryton email notification
Classifier: Development Status :: 5 - Production/Stable
Classifier: Environment :: Plugins
Classifier: Framework :: Tryton
Classifier: Intended Audience :: Developers
Classifier: License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)
Classifier: Natural Language :: Bulgarian
Classifier: Natural Language :: Catalan
Classifier: Natural Language :: Chinese (Simplified)
Classifier: Natural Language :: Czech
Classifier: Natural Language :: Dutch
Classifier: Natural Language :: English
Classifier: Natural Language :: Finnish
Classifier: Natural Language :: French
Classifier: Natural Language :: German
Classifier: Natural Language :: Hungarian
Classifier: Natural Language :: Indonesian
Classifier: Natural Language :: Italian
Classifier: Natural Language :: Persian
Classifier: Natural Language :: Polish
Classifier: Natural Language :: Portuguese (Brazilian)
Classifier: Natural Language :: Romanian
Classifier: Natural Language :: Russian
Classifier: Natural Language :: Slovenian
Classifier: Natural Language :: Spanish
Classifier: Natural Language :: Turkish
Classifier: Natural Language :: Ukrainian
Classifier: Operating System :: OS Independent
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: 3.8
Classifier: Programming Language :: Python :: 3.9
Classifier: Programming Language :: Python :: 3.10
Classifier: Programming Language :: Python :: 3.11
Classifier: Programming Language :: Python :: 3.12
Classifier: Programming Language :: Python :: Implementation :: CPython
Classifier: Topic :: Office/Business
Requires-Python: >=3.8
License-File: LICENSE
Requires-Dist: Genshi
Requires-Dist: python-sql
Requires-Dist: trytond<7.1,>=7.0
Provides-Extra: test
Requires-Dist: trytond_company<7.1,>=7.0; extra == "test"
Requires-Dist: trytond_commission<7.1,>=7.0; extra == "test"
Requires-Dist: trytond_party<7.1,>=7.0; extra == "test"
Requires-Dist: trytond_web_user<7.1,>=7.0; extra == "test"

Notification Email Module
#########################

The notification email module allows to define email templates which will be
sent to a list of recipients when a trigger is fired on a record event.
Extra reports from the same record can be attached to the email.

Configuration
*************

The notification_email module uses parameters from the section:

- ``[notification_email]``:

    - ``from``: The default ``From`` for the email.

